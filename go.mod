module gitlab.com/golang-gopher/go-rest-websocket

go 1.21.0

require (
	github.com/golang-jwt/jwt/v5 v5.0.0
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/websocket v1.5.0
	github.com/joho/godotenv v1.5.1
	github.com/lib/pq v1.10.9
	github.com/rs/cors v1.10.0
	github.com/segmentio/ksuid v1.0.4
	golang.org/x/crypto v0.13.0
)
